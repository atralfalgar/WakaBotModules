WakaBotModules - Extra (2) modules collection
==================================

All the modules which are way too strange to figure on the main repository or even on the secondary repository.

Dos
-------

Module for displaying Divinity Original Sin fortunes.
If an argument is given, it will be considered as the server token and will be displayed in the message.
