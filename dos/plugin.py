# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Random
from random import randint

# Regex
from re import match

# Waka
from waka.module import WakaModule

# ------------------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------------------

class Module(WakaModule):

    def __init__(self, *args, **kargs):
        """ Constructor
        """

        super().__init__(*args, **kargs)

        self.dos_msg = []

        with open(self.filepath, encoding="utf8") as data_file:
            data = data_file.readlines()
        self.dos_msg = [x.strip() for x in data]
        

    def call(self):
        """ Call this method from channels

        Returns
        -------
        str or None
            Response message
        """

        dos_fortune = self.dos_msg[ randint(0, len(self.dos_msg) - 1) ]

        messages = self.get_messages()

        if len(messages) > 2:

            # A server user and a token were given, let's show them
            return dos_fortune + " " + self.fortune_separator + " " + self.get_current_user() + " " + self.invite_text + " " + messages[1] + " ! " + self.token_text + " " + messages[2]
        
        elif len(messages) > 1:

            # A token has been given, let's show it
            return dos_fortune + " " + self.fortune_separator + " " + self.invite_token_only_text + " " + messages[1]
            
        # No token, let's only return the fortune message
        return dos_fortune
        
    
